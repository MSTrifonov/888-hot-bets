import React from 'react'

export const HotIcon = () => (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 33.3 21.08">
        <path d="M33.3 6.4A19.06 19.06 0 0 0 26.6 2C23.2.6 19.3 3.8 16.4 0c.4 2.8 1.5 4.5 2.9 5 1.1.4 2.5.3 3.4.7a2 2 0 0 1 .8 3c-2.6 4.8-6.8-2.4-10.2-2.6-4.6-.3-8.9 4-13.3 0 3.6 12.6 14.8.6 14.3 9.2-.2 3.3-5.6 1.6-8.8.3.7 4 5.4 5.9 8.4 5.4a107.17 107.17 0 0 1 19.3-1.3V6.4h.1z" fill="#ff6d00"></path>
    </svg>
)