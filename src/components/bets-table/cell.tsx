import React from 'react';

export type TCellPath = { column: string | number, row: string | number }
export interface ICell {
    value: any,
    nameCSS?: string,
    onClick?: (path: TCellPath) => void,
    path: TCellPath
}

/**
 * Компонента вызова ячейки в таблице
 * Тип: Функциональная
 * Мемоизация: Да
 * @param {any} value Значение передаваемое в ячейку
 * @param {string} [nameCSS] наименование имени класса
 * @param {func} [onClick] обработчик нажатия
 * @param {number} path.column Расположение ячейки относительно колонки
 * @param {number} path.row Расположение ячейки относительно строки
 * @return <td>'CELL'</td>
 */
const Cell = ({ value, nameCSS, onClick, path }: ICell) => {
    return (
        <td className={nameCSS} onClick={() => { if (onClick) return (onClick(path)) }}>
            {value}
        </td>
    )
}

function areEqual(prevProps: any, nextProps: any): any {
    return ((JSON.stringify(prevProps) === JSON.stringify(nextProps)))
}

export default React.memo(Cell, areEqual)