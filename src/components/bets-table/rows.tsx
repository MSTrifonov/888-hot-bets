import React from 'react'
import Cell, { TCellPath } from './cell';

export type TRowData = { value: any, cellCSS?: string }

export interface IRow {
    data: TRowData[],
    onClickCell?: (path: TCellPath) => void,    
    indexRow: number,
    rowCSS?: string,
}

/**
 * Компонента вызова строки в таблице
 * Тип: Функциональная
 * Мемоизация: Да
 * @param {object[]} data Массив ячеек в строке
 * @param {any} data.value Значение передаваемое в ячейку
 * @param {string} [data.cellCSS] наименование имени класса
 * @param {func} [onClickCell] обработчик клика по ячейки
 * @param {string} [rowCSS] наименование имени класса
 * @param {number} indexRow индекс строки 
 * @return <tr>'ROW'</tr>
 */
const Row = ({ data, onClickCell, rowCSS, indexRow }: IRow) => {
    return (
        <tr className={rowCSS}>
            {data.map((item, indexColl) =>
                item && <Cell key={`Cell${indexColl}${indexRow}`} value={item.value} onClick={onClickCell} path={{ column: indexColl, row: indexRow }} nameCSS={item.cellCSS} />
            )}
        </tr>
    )
}

function areEqual(prevProps: any, nextProps: any): any {
    return ((JSON.stringify(prevProps) === JSON.stringify(nextProps)))
}

export default React.memo(Row, areEqual)