/** @jsx jsx */
import { jsx, css } from '@emotion/core';
import React from 'react';

const sTab = (isActive :boolean) => css`
    margin: 0 5px;
    padding: 0 5px;
    color: ${isActive? '#ebd700' : '#d5d5d8'};
    :hover{
        color: #ebd700;
    }
`

interface IProps{
    item: {id: number, data: string}
    index: number 
    isActive: boolean    
    tabsOnClick: (index :number) => void, 
}

/**
 * Компонента вызова элемнта Tab
 * Тип: Функциональная
 * Мемоизация: Да
 * @param {object} item Данные элемента Tab
 * @param {number} item.id Индификатор элемента
 * @param {any} item.data Отображаемые данные
 * @param {func} tabsOnClick Обработчик нажатия
 * @param {number} index Индекс активного Tab
 * @param {boolean} isActive Сейчас активен?
 * @returns <span>'TAB'</span>
 */

const Tab = ({item, tabsOnClick, index, isActive} :IProps) => {
    return (<span key={`Tab${item.id}`} css={sTab(isActive)} onClick={() => tabsOnClick && tabsOnClick(index)}>{item.data}</span>)
}

function areEqual(prevProps :any, nextProps :any) :any {    
    return((JSON.stringify(prevProps)===JSON.stringify(nextProps)))
}

export default React.memo(Tab, areEqual)