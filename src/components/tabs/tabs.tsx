import React from 'react'
/** @jsx jsx */
import { jsx } from '@emotion/core';
import { TTabList } from "../../redux/hot-bets/types";
import Tab from "./tab";

type TProps = {
    icon: JSX.Element,
    activeTab: number,
    tabList?: TTabList[],
    sHeader?: any
    tabsOnClick: (index: number) => void,
}

/**
 * Компонента вывода панели вкладок
 * Тип: Функциональная
 * Мемоизация: Да 
 * @param {image} icon эконка в левом верхнем углу
 * @param {Object[]} [tabList] массив с данными всех элементов Tab
 * @param {number} tabList.id Индификатор элемента
 * @param {any} tabList.data Отображаемые данные
 * @param {func} tabsOnClick Обработчик клика по Tab
 * @param {number} activeTab Активная вкладка
 * @param {any} [sHeader] стилизация Tab Panel
 * @returns <span>'TAB PANEL'</span>
 */
const Tabs = ({ icon, tabList, tabsOnClick, activeTab, sHeader }: TProps) => {
    const tabGenerate = () => {
        return (
            <span>
                {tabList && tabList.map((item, index) =>
                    <Tab key={`Tab${item.id}`} item={item} index={index} tabsOnClick={tabsOnClick} isActive={index === activeTab} />
                )}
            </span>
        )
    }
    return (
        <span css={sHeader}>
            <span className='tabPanel'>
                <span>{icon}</span>
                <span className='title'>Горячие ставки</span>
                {tabGenerate()}
            </span>
        </span>
    )
}

function areEqual(prevProps :any, nextProps :any) :any {    
    return((JSON.stringify(prevProps)===JSON.stringify(nextProps)))
}

export default React.memo(Tabs, areEqual)
