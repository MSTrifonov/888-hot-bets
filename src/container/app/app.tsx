import { lazy, Suspense } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Preloader from '../../components/preloader/preloader';
/** @jsx jsx */
import { jsx, css } from '@emotion/core';

const MainPage = lazy(() => import('../bets-widget/bets-widget'))
const Page404 = lazy(() => import('../../page/page404/page404'))

const sBackground = css`
  background: #282836;
  height: 100%;
`

function App() {
  return (
    <div css={sBackground}>
    <Router>
      <Suspense fallback={<Preloader />}>
        <Switch>
          <Route path="/" exact component={MainPage} />
          <Route component={Page404} />
        </Switch>
      </Suspense>
    </Router>
    </div>
  );
}

export default App;
