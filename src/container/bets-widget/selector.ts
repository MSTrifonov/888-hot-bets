import { createSelector } from 'reselect';
import { TRowData } from '../../components/bets-table/rows';
import { TABLE_HEAD } from './settings';

export const sportListSelector = createSelector(
    [(state: any) => state.sportList, (state: any) => state.indexView],
    (itemList, index) => {
        const data = itemList && itemList[index] && itemList[index].gameList

        //generate header
        let tableHead: TRowData[] = []
        data && (Object.entries(TABLE_HEAD).forEach(([key, value]) => data[0][key] && tableHead.push({ value: value.name, cellCSS: value.headerCSS })))

        return (itemList && {
            tabList: itemList.map((item: any) => ({ id: item.id, data: item.name })),
            tableHead,
            data: data.map((item: any) => ({
                id: item.id,
                data: [
                    { name: 'time', value: new Date(item.time).toTimeString().replace(/:[0-9]{2,2} .*/, ''), cellCSS: TABLE_HEAD.time.cellCSS },
                    { name: 'team1', value: item.team1, cellCSS: TABLE_HEAD.team1.cellCSS },
                    { name: 'team2', value: item.team2, cellCSS: TABLE_HEAD.team2.cellCSS },
                    { name: 'P1', value: item.P1.price, id: item.P1.id, cellCSS: TABLE_HEAD.P1.cellCSS },
                    item.X && { name: 'X', value: item.X.price, id: item.X.id, cellCSS: TABLE_HEAD.X.cellCSS },
                    { name: 'P2', value: item.P2.price, id: item.P2.id, cellCSS: TABLE_HEAD.P2.cellCSS },
                    { name: 'countOptionalMarkets', value: `+${item.countOptionalMarkets}`, cellCSS: TABLE_HEAD.countOptionalMarkets.cellCSS },
                ]
            })),
        })
    })