// Table setting
export const TABLE_HEAD = {
    time: { name: 'Время', cellCSS: '', headerCSS: '' },
    team1: { name: 'Команда 1', cellCSS: 'ra', headerCSS: 'ra' },
    team2: { name: 'Команда 2', cellCSS: 'la', headerCSS: 'la' },
    P1: { name: 'П1', cellCSS: 'onClick', headerCSS: '' },
    X: { name: 'Х', cellCSS: 'onClick', headerCSS: '' },
    P2: { name: 'П2', cellCSS: 'onClick', headerCSS: '' },
    countOptionalMarkets: { name: '', cellCSS: '', headerCSS: '' }
}