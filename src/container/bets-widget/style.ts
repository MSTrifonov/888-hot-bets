import { css } from '@emotion/core';

// STYLING
export const sWidget = css`
    display: flex;
    flex-direction: column;
    width: fit-content;
`

export const sPage = css`
    padding: 40px;
`

export const sTable = css`
    background: #23232f;
    max-height: 324px;
    color: #d5d5d8;
    font-size: 12px;
    line-height: 21px;
    .row-header {
        background: #121317;
    }

    td{
        padding: 5px;
        text-align: center;
    };

    .onClick{
        background: #1e1e28;
        :hover{
            background: #50505e
        };
    };

    .la{
        text-align: left;
    };

    .ra{
        text-align: right;
    };
`

export const sHeader = css`
    background: #2f2f3d;    
    padding: 0 18px;
    height: 50px;
    display: flex;
    align-items: center;

    .title {    
        text-align: left;
        font-size: 16px;
        margin: 0px 40px 0px  10px;
        color: #fff;
    }    
    
    svg {height: 16px;};
`