import React from 'react';
import ReactDOM from 'react-dom';
import App from './container/app/app';
import { store } from './redux';
import { Provider } from 'react-redux';
import ReduxToastr from 'react-redux-toastr';


import 'react-redux-toastr/lib/css/react-redux-toastr.min.css';

ReactDOM.render(
    <Provider store={store} >
        <App />
        <ReduxToastr
            timeOut={4000}
            newestOnTop={false}
            preventDuplicates
            position="top-right"
            transitionIn="fadeIn"
            transitionOut="fadeOut"
            progressBar
            closeOnToastrClick />
    </Provider>
    , document.getElementById('root'));

