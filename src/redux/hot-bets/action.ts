import { ISportItem, SET_SPORT_LIST, TTabList, SET_TAB_LIST, SET_INDEX_VIEW } from "./types";
import { ActionCreator, Action } from "redux";

export const setSportList: ActionCreator<Action> = (sportList :ISportItem[]) =>{
    return{
        type: SET_SPORT_LIST,
        payload: sportList
    }
}

export const setTabList: ActionCreator<Action> = (tabList :TTabList[]) =>{
    return{
        type: SET_TAB_LIST,
        payload: tabList
    }
}

export const setIndexView: ActionCreator<Action> = (index :number) =>{
    return{
        type: SET_INDEX_VIEW,
        payload: index
    }
}
    
    