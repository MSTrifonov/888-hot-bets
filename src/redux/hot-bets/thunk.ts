import { Action, ActionCreator, Dispatch } from 'redux';
import {ThunkAction} from 'redux-thunk';
import { setSportList} from './action';
import { getSportList } from '../../service/fake-api';

export const dataLoadTC :ActionCreator<ThunkAction<void, Action<any> , void, Action<any>>> = () => async (dispatch: Dispatch<Action<any>>) => {
    console.time('parseJSON')
    const response = await getSportList()     
    console.timeEnd('parseJSON')
    dispatch(setSportList(response))
}



