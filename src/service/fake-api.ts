import fakeData from './data.json'
import { IGame, TEvent } from '../redux/hot-bets/types.js';
import { ISportItem } from './../redux/hot-bets/types';

type TEntries = [string?, any?]
type TEventObj = { [key: string]: TEvent }
const COUNT_TAB = 5
const COUNT_ROW = 8;

const eventParse = (data: any) => {
    if (!data) {
        return undefined
    }
    const event = data.event;
    let eventObj: TEventObj = {}
    Object.entries(event).forEach(([, item]: TEntries) => Object.assign(eventObj, { [item.type]: { id: item.id, price: item.price, name: item.name } }))

    return eventObj
}

const sortSportItem = (SportList: ISportItem[]) => {
    SportList.sort((a, b) => b.countGame - a.countGame)
    return SportList.slice(0, COUNT_TAB).map(item => ({ ...item, gameList: item.gameList.sort((a, b) => a.time - b.time).slice(0,COUNT_ROW)}))
}


export function getSportList(isReject = false) {
    if (isReject) {
        return (Promise.reject())
    }

    const sportList = fakeData.sport;

    let res = Object.values(sportList).map(({ id, name, region }: any) => {
        let gameList: IGame[] = []
        Object.entries(region).forEach(([, itemRegion]: TEntries) => {
            Object.entries(itemRegion.competition).forEach(([, itemComp]: TEntries) => {
                Object.entries(itemComp.game).forEach(([, itemGame]: TEntries) => {
                    const event = eventParse((Object.values(itemGame.market).find((obj: any) => obj.type === "MatchResult")))
                    
                    event && gameList.push({
                        id: itemGame.id,
                        time: itemGame.start_ts,
                        team1: itemGame.team1_name,
                        team2: itemGame.team2_name,
                        P1: event.W1,
                        [event.X && 'X']: event.X,
                        P2: event.W2,
                        countOptionalMarkets: itemGame.markets_count,
                    }) 
                })
            })
        })
        return ({ id, name, gameList, countGame: gameList.length })
    })

    return (Promise.resolve(sortSportItem(res)))
}